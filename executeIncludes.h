/*
 * executeIncludes.h
 *
 *  Created on: 2017-02-01
 *      Author: etudiant
 */

#ifndef EXECUTEINCLUDES_H_
#define EXECUTEINCLUDES_H_

#include <string>
#include <set>
#include <fstream>

std::string executeIncludes(std::string const & file_name,  std::set<std::string> = std::set<std::string>{});


#endif /* EXECUTEINCLUDES_H_ */
