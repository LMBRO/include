/*
 * executeIncludes.cpp
 *
 *  Created on: 2017-02-01
 *      Author: etudiant
 */
#include "executeIncludes.h"
const std::string INCLUDE_CONST = "#include ";
const std::string FNAME_MOD_PREFIX = "MODIFIED";

//caveat for now : only one space allowed between en of #include and star of <filename>
std::string executeIncludes(std::string const & file_name, std::set<std::string> seen_fnames) {
	if (seen_fnames.find(file_name) != seen_fnames.end())
		throw std::invalid_argument { "The " + file_name + " file includes itself." };
	else
		seen_fnames.insert(file_name);
	std::ifstream ifs;
	ifs.unsetf(std::ios_base::skipws);
	ifs.open(file_name);
	if(!ifs.good())
		throw std::invalid_argument{ "The included file "+ file_name + " isn't accessible." };
	std::ofstream ofs;
	ofs.unsetf(std::ios_base::skipws);
	std::string newfname = FNAME_MOD_PREFIX + file_name;
	ofs.open(newfname, std::ofstream::out | std::ofstream::trunc);
	char c;
	std::string buffer;
	while (ifs >> c) {
		buffer = "";
		if (c == '#') {
			//then the search for potential include statement starts here
			buffer += c;
			unsigned int include_index = 1;
			bool first_part_invalid = false;
			while (ifs >> c and include_index < INCLUDE_CONST.length() and !first_part_invalid) {
				if (INCLUDE_CONST[include_index] != c) {
					first_part_invalid = true;
					break;
				} else {
					buffer += c;
					include_index++;
				}
			}
			if (!ifs) {
				// while trying to complete the #include part of the include statement, eof reached, so the function ends.
				ofs << buffer;
				break;
			} else if (first_part_invalid) {
				// Wasn't actually an include
				ofs << buffer;
				continue;
			}
			// the #include part of the include statement was successfully recognized
			else {
				if (c != '"') {
					ofs << buffer << c;
					continue;
				} else {
					buffer += "\"";
					bool incld_fname_is_complete = false;
					std::string included_fname = "";
					while (ifs >> c and c != '\n' and !incld_fname_is_complete) {
						if (c == '"') {
							incld_fname_is_complete = true;
							break;
						} else {
							buffer += c;
							included_fname += c;
						}
					}
					//No multiline litterals allowed, so dump the buffer and continue
					if (c == '\n') {
						ofs << buffer << c;
						continue;
					}
					//Couldn't complete filename before eof
					else if (!ifs) {
						ofs << buffer;
						break; // or continue
					} else if (incld_fname_is_complete) {
						std::string included_execd_fname = executeIncludes(included_fname, seen_fnames);
						std::ifstream ifs_incld;
						ifs_incld.unsetf(std::ios_base::skipws);
						ifs_incld.open(included_execd_fname);
						char c;
						while (ifs_incld >> c) {
							ofs << c;
						}
					}
				}
			}
		} else {
			ofs << c;
		}
	}
	return newfname;
}

